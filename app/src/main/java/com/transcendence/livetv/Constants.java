package com.transcendence.livetv;

/**
 * Created by Antonio on 09-Sep-16.
 */
public class Constants {

    public static final String TAG_CHANNEL = "channel";

    public static final String TAG_SITEL = "Sitel";
    public static final String TAG_KANAL5 = "Kanal5";
    public static final String TAG_KANAL5PLUS = "Kanal5P";
    public static final String TAG_TELMA = "Telma";
    public static final String TAG_ALFA = "Alfa";

    public static final String URI_SITEL = "http://217.16.82.6:1935/zulu/sitel_2/playlist.m3u8";
    public static final String URI_KANAL5 = "http://217.16.82.6:1935/zulu/kanal5_2/playlist.m3u8";
    public static final String URI_KANAL5PLUS = "http://217.16.82.6:1935/zulu/kanal5plus_2/playlist.m3u8";
    public static final String URI_TELMA = "http://217.16.82.6:1935/zulu/telma_2/playlist.m3u8";
    public static final String URI_ALFA = "http://217.16.82.6:1935/zulu/alfa_2/playlist.m3u8";
    public static final String URI_MRT = "http://217.16.82.6:1935/zulu/mtv1_2/playlist.m3u8";

}
