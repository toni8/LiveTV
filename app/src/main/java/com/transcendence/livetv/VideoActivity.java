package com.transcendence.livetv;


import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.transition.Fade;
import android.support.transition.TransitionManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Slide;
import android.transition.TransitionInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.devbrackets.android.exomedia.core.video.scale.ScaleType;
import com.devbrackets.android.exomedia.listener.OnPreparedListener;
import com.devbrackets.android.exomedia.ui.widget.EMVideoView;
import com.devbrackets.android.exomedia.ui.widget.VideoControls;

import java.util.ArrayList;
import java.util.List;


public class VideoActivity extends AppCompatActivity {

    Boolean paused = false, fullScreen = false;
    TvChannel channel = new TvChannel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_view_layout);

        //   getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ViewGroup mRootView = (ViewGroup) findViewById(R.id.rel_layout_video_view);
        Fade mFade = new Fade(Fade.IN);
        TransitionManager.beginDelayedTransition(mRootView, mFade);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                channel = null;
            } else {
                channel = (TvChannel) extras.getSerializable(Constants.TAG_CHANNEL);
            }
        } else {
                channel = (TvChannel) savedInstanceState.getSerializable(Constants.TAG_CHANNEL);
        }

        setupVideoView();
    }

    private void setupVideoView() {
        final EMVideoView emVideoView = (EMVideoView) findViewById(R.id.video_view);
        emVideoView.setScaleType(ScaleType.FIT_CENTER);

        VideoControls vc = emVideoView.getVideoControls();
        vc.setTitle(channel.getTitle());
        vc.setFastForwardButtonRemoved(false);
        vc.setFastForwardButtonEnabled(true);

        emVideoView.setOnPreparedListener(new OnPreparedListener() {
            @Override
            public void onPrepared() {
                emVideoView.start();
            }
        });

        emVideoView.setVideoURI(Uri.parse(channel.getUriString()));
    }


    private void hideStatusBar() {

        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }

    }

    private void showStatusBar() {

        if (Build.VERSION.SDK_INT < 16) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_VISIBLE;
            decorView.setSystemUiVisibility(uiOptions);
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(Constants.TAG_CHANNEL, channel);

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if(savedInstanceState != null)
            channel = (TvChannel) savedInstanceState.getSerializable(Constants.TAG_CHANNEL);
    }
}
