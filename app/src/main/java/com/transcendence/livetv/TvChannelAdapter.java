package com.transcendence.livetv;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Antonio on 08-Sep-16.
 */
public class TvChannelAdapter extends RecyclerView.Adapter<TvChannelAdapter.ViewHolder> {

    private List<TvChannel> items;
    private int itemLayout;

    public TvChannelAdapter(List<TvChannel> items, int itemLayout) {
        this.items = items;
        this.itemLayout = itemLayout;
    }

    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new ViewHolder(v);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {
        TvChannel item = items.get(position);
        holder.text.setText(item.getTitle());
        holder.image.setImageResource(item.getIcon());
    }

    @Override public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;
        public TextView text;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.icon);
            text = (TextView) itemView.findViewById(R.id.title);
        }
    }
}
