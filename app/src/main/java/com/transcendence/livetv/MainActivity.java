package com.transcendence.livetv;


import android.net.Uri;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;


import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import com.devbrackets.android.exomedia.core.video.scale.ScaleType;
import com.devbrackets.android.exomedia.listener.OnPreparedListener;
import com.devbrackets.android.exomedia.ui.widget.EMVideoView;
import com.devbrackets.android.exomedia.ui.widget.VideoControls;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    List<TvChannel> channels = new ArrayList<>();
    TvChannel selectedChannel = new TvChannel();
    EMVideoView emVideoView;
    RecyclerView recyclerView;
    VideoControls vc;

    Button btn_view_channels;
    Button btn_close_channels;

    Animation bottomUp, bottomDown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottomUp = AnimationUtils.loadAnimation(MainActivity.this,
                R.anim.bottom_up);

        bottomDown = AnimationUtils.loadAnimation(MainActivity.this,
                R.anim.bottom_down);

        addChannels();

        View extraView = LayoutInflater.from(this).inflate(R.layout.extra_view, null);
        btn_view_channels = (Button) extraView.findViewById(R.id.btn_view_channels);

        btn_close_channels = (Button) findViewById(R.id.btn_close_channels);

        emVideoView = (EMVideoView) findViewById(R.id.video_view);
        emVideoView.setScaleType(ScaleType.FIT_CENTER);
        vc = emVideoView.getVideoControls();
        vc.addExtraView(extraView);
        vc.getExtraViews().get(0).getLayoutParams().height = 85;
        vc.getExtraViews().get(0).getLayoutParams().width = 85;

        vc.hideDelayed(100);
        //vc.setVisibility(View.INVISIBLE);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(new TvChannelAdapter(channels, R.layout.custom_row));
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                /*TvChannel channel = channels.get(position);
                Intent i = new Intent(MainActivity.this, VideoActivity.class);
                i.putExtra(Constants.TAG_CHANNEL, channel);

                Fade  mFade = new Fade(Fade.OUT);
                TransitionManager.beginDelayedTransition(recyclerView, mFade);

                startActivity(i);*/
                recyclerView.startAnimation(bottomDown);
                recyclerView.setVisibility(View.GONE);
                btn_close_channels.setVisibility(View.GONE);
                vc.setVisibility(View.VISIBLE);
                selectedChannel = channels.get(position);
                setupVideoView();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        btn_view_channels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vc.hideDelayed(100);
               // vc.setVisibility(View.INVISIBLE);

                recyclerView.startAnimation(bottomUp);
                recyclerView.setVisibility(View.VISIBLE);
                btn_close_channels.setVisibility(View.VISIBLE);
            }
        });

        btn_close_channels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerView.startAnimation(bottomDown);
                recyclerView.setVisibility(View.GONE);
                btn_close_channels.setVisibility(View.GONE);
                vc.setVisibility(View.VISIBLE);
            }
        });

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                selectedChannel = null;
            } else {
                selectedChannel = (TvChannel) extras.getSerializable(Constants.TAG_CHANNEL);
            }
        } else {
            selectedChannel = (TvChannel) savedInstanceState.getSerializable(Constants.TAG_CHANNEL);
        }


        setupVideoView();

    }

    private void setupVideoView() {

        if (selectedChannel != null) {

            vc.setTitle(selectedChannel.getTitle());

            emVideoView.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared() {
                    emVideoView.start();
                }
            });

            emVideoView.setVideoURI(Uri.parse(selectedChannel.getUriString()));
        }
    }


    private void addChannels() {

        TvChannel channel = new TvChannel(R.drawable.logo_mrt, "МРТ", Constants.URI_MRT);
        channels.add(channel);

        channel = new TvChannel(R.drawable.logo_sitel, "Сител", Constants.URI_SITEL);
        channels.add(channel);

        channel = new TvChannel(R.drawable.logo_kanal5, "Канал 5", Constants.URI_KANAL5);
        channels.add(channel);

        channel = new TvChannel(R.drawable.logo_kanal5_plus, "Канал 5 Плус", Constants.URI_KANAL5PLUS);
        channels.add(channel);

        channel = new TvChannel(R.drawable.logo_telma, "Телма", Constants.URI_TELMA);
        channels.add(channel);

        channel = new TvChannel(R.drawable.logo_alfa, "Алфа", Constants.URI_ALFA);
        channels.add(channel);


    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(Constants.TAG_CHANNEL, selectedChannel);

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState != null)
            selectedChannel = (TvChannel) savedInstanceState.getSerializable(Constants.TAG_CHANNEL);
    }

}
