package com.transcendence.livetv;

import android.net.Uri;
import android.widget.ImageView;

import java.io.Serializable;

/**
 * Created by Antonio on 08-Sep-16.
 */
public class TvChannel implements Serializable {

    int icon;
    String title;
    String uriString;

    public TvChannel(int icon, String title, String uriString) {
        this.icon = icon;
        this.title = title;
        this.uriString = uriString;
    }

    public TvChannel() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getUriString() {
        return uriString;
    }

    public void setUriString(String uriString) {
        this.uriString = uriString;
    }


}
